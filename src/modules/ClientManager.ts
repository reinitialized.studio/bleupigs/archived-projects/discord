import Discord from 'discord.js';
import config from './Config';

const client = new Discord.Client();

client.login(config.bot_token).then(()=>{
    console.log("Bot online!");
});

export default client;
