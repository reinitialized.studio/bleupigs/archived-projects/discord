import Discord from 'discord.js';
import Command, {Arguments,ArgumentTypes} from '../types/Commands';

interface CommandArguments extends Arguments {
	first: string,
	second: string,
	optional?: string,
	other: string
}

const cmd:Command = {
	CommandName: 'words',
	Arguments: [
		{
			id:'first',
			type: ArgumentTypes.string,
			required: true,
		},
		{
			id:'second',
			type: ArgumentTypes.string,
			required: true,
		},
		{
			id:'optional',
			type: ArgumentTypes.string,
			required: false,
		},
		{
			id:'other',
			type: ArgumentTypes.string,
			required: true,
		},
	],
	run(args:CommandArguments,message:Discord.Message){
		return `First word: ${args.first}\nSecond word: ${args.second}${args.optional ? `\nThird word:${args.optional}` : ''}\nOther words: ${args.other}`;
	}
}

module.exports = cmd;