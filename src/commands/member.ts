import Discord from 'discord.js';
import Command, {Arguments,ArgumentTypes} from '../types/Commands';

interface CommandArguments extends Arguments {
	member: Discord.GuildMember
}

const cmd:Command = {
	CommandName: 'member',
	Arguments: [
		{
			id: 'member',
			type: ArgumentTypes.GuildMember,
			required: true,
		},
	],
	run(args:CommandArguments,message:Discord.Message){
		console.log(args);
		if(args.member){
			return args.member.user.username+'#'+args.member.user.discriminator;
		}else{
			return 'No member found'
		}
	}
}

module.exports = cmd;