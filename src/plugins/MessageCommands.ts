import Discord from 'discord.js';
import client from '../modules/ClientManager';
import config from '../modules/Config';

import {GetCommand,LoadCommands} from '../modules/CommandManager';
import {Arguments,ArgumentTypes} from '../types/Commands';

LoadCommands();

const WhitelistedGuild = '758406496563625996';

async function ParseArgument(str:string,type:ArgumentTypes,message:Discord.Message)
	:Promise<string | number | boolean | Discord.GuildMember | Discord.Role | null>
{
	switch(type){
		case ArgumentTypes.string: {
			return str;
		}
		case ArgumentTypes.number: {
			const num = parseFloat(str);
			return isNaN(num) ? null : num;
		}
		case ArgumentTypes.boolean: {
			switch(str.toLowerCase()){
				case "true":
				case "yes":
				case "1":
					return true;
				case "false":
				case "no":
				case "0":
					return false;
				default:
					return null;
			}
		}
		case ArgumentTypes.GuildMember: {
			const idMatch = /^(?:<@|<@!)?([0-9]+)(?:>)?$/;
			const match = str.match(idMatch);
			return (match && message.guild) ? (message.guild.member(match[1])) : null;
		}
		case ArgumentTypes.Role: {
			const idMatch = /^(?:<@&)?([0-9]+)(?:>)?$/;
			const match = str.match(idMatch);
			return (match && message.guild) ? (await message.guild.roles.fetch(match[1])) : null;
		}
	}
	return null;
}

client.on('message', async (message:Discord.Message) => {
	if(message.author.bot)return;

	if(!message.guild || message.guild.id !== WhitelistedGuild)return;
	let content = message.content;
	if (content[0]!==config.prefix){
		// checks the ping at the start which can be used instead of prefix
		// redundant code, should figure out how to combine it
		const BotID = client.user?.id;
		if(!BotID)return;
		if(content.startsWith('<@!')){
			if(content.substring(3,BotID.length+3)===BotID && content[BotID.length+3]==='>'){
				content = content.substring(4+BotID.length).trim();
			}else{
				return;
			}
		}else if(content.startsWith('<@')){
			if(content.substring(2,BotID.length+2)===BotID && content[BotID.length+2]==='>'){
				content = content.substring(3+BotID.length).trim();
			}else{
				return;
			}
		}else{
			return;
		}
	}else{
		content = content.substring(1);
	}

	const args = content.split(config.separator);
	const cmd = (args.shift() ?? '').toLowerCase();

	const command = GetCommand(cmd);
	if(command === undefined){
		// User Error: no command found
		return;
	}

	const ParsedArguments:Arguments = {};
	if(command.Arguments){
		const n = command.Arguments.length;
		const len = args.length;
		let iterator = len;
		while(args[n]){
			const last = args.pop() ?? '';
			args[(--iterator)-1] += config.separator + last;
		}

		let RequiredArguments = 0;
		for(const a of command.Arguments){
			if(a.required){
				RequiredArguments++;
			}
		}

		if(len < RequiredArguments){
			// User Error: not enough arguments provided
			return;
		}

		// Parse arguments that are not required until we run out of "budget"
		// after that, parse only required arguments
		let NotRequiredArgumentsBudget = len - RequiredArguments;
		iterator = 0;
		for(const ArgumentData of command.Arguments){
			const StringArgument = args[iterator];
			if(ArgumentData.required || NotRequiredArgumentsBudget>0){
				const Parsed = await ParseArgument(StringArgument,ArgumentData.type,message);
				if(!Parsed){
					if(ArgumentData.required){
						// User Error: Couldn't parse required argument
						return;
					}else{
						continue;
					}
				}
				ParsedArguments[ArgumentData.id] = Parsed;
				if(!ArgumentData.required){
					NotRequiredArgumentsBudget--;
				}
				iterator++;
			}
		}
	}

	const result = command.run(ParsedArguments,message);
	if(result){
		message.channel.send(result,{
			allowedMentions: {
				parse: [],
				users: [message.author.id]
			}
		});
	}
});